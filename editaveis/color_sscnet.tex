\chapter[Color SSCNet]{Color SSCNet}
\label{ch:color_sscnet}

Apesar do mapa de profundidade possuir informações importantes quanto à geometria da cena ele é muitas vezes uma fonte de dados ruidosa, possuindo falhas na leitura que omitem parte da superfície observável. Além disso, quando apenas à informação de profundidade é utilizada, ambiguidades entre objetos podem ocorrer, como discutido previamente em \ref{subsec:combining}. Buscando solucionar tais problemas nesse trabalho é proposta uma modificação na rede SSCNet para incorporar também informações de cores (Color SSCNet).

\section{Adicionando canais de cores}

Duas modificações na arquitetura da rede SSCNet foram abordadas para a fusão dos dados de profundidade:

\begin{itemize}
\item Adicionar as informações de cores como novos canais da camada de entrada onde o primeiro canal é referente aos dados do fTSDF e outros três canais referentes aos canais de cores \textit{Red}, \textit{Green} e \textit{Blue} respectivamente.
\item Realizar uma fusão ``mid-level'' onde parte da rede utilizada para os mapas de profundidade é replicada e utilizada para aprender a representação local dos dados de cores onde posteriormente é concatenada ao galho principal antes das camadas convolucionais finais da rede.
\end{itemize}

A codificação de entrada das informações de cores foi comum às duas formas apresentadas, onde cada canal de cor corresponde a um volume 3D discreto composto por voxels que armazenam a intensidade da cor ou a ausência da informação de cor naquele ponto do espaço.

Esses volumes foram construídos à partir da projeção do mapa de profundidade no espaço tri-dimensional, dadas as informações de pose do sensor RGBD. Dessa forma, dado que o volume possua resolução suficiente, para cada pixel do mapa de profundidade existe um voxel correspondente no volume derivado. As imagens RGB, por sua vez, possuem um alinhamento bem próximo de seus mapas de profundidade correspondentes, de forma que os objetos em ambas representações estão (com uma mínima margem de erro) nas mesmas posições. Baseando-se nessa premissa, é então possível determinar qual pixel da imagem de cor corresponde a um voxel do volume de entrada, possibilitando então uma codificação tri-dimensional da disposição das cores no espaço. Essa codificação, porém, deixa vários voxels sem informação, pois tanto o mapa de profundidade quanto a imagem RGB possuem apenas informações da superfície observável. Sabendo disso os volumes de cor foram inicializados com o valor -1 indicando espaço livre ou regiões oclusas. As informações de cor, para conformarem com a escala de codificação do fTSDF, foram codificadas com valores reais entre 0 e 1, onde os limites correspondem à menor e à maior intensidade de cor respectivamente.

%%% Descrever arquiteturas early fusion e mid-level fusion

\section{Limitação de memória}

Segundo Song et al. a rede SSCNet proposta tinha como requisito mínimo de treinamento uma GPU com 12 GB de memória, sendo necessários aproximadamente 5.8 GB para carregar a rede para testes e 11.8 GB para treinamento. GPUs que possuem essa capacidade estão no topo de linha dentre as amplamente disponíveis no mercado, dessa forma o risco de exaustão de recursos devido à limitação de memória foi levado em consideração quando proposta a adição da informação de cor.

Esse risco chegou a se tornar uma falha durante os experimentos iniciais com os canais adicionais de cor, onde a rede proposta ultrapassou 12 GB de memória. Entretanto, devido à uma investigação mais minuciosa do consumo de memória da rede, foi identificado um gasto desnecessário nas configurações de treinamento, onde uma rede de teste era iniciada em conjunto com a rede de treinamento para fazer uma avaliação iterativa do desempenho de inferência da rede. Sendo a avaliação não essencial para o treinamento da rede, a configuração foi atualizada para desativar tal opção. Com a desativação desse recurso, a rede proposta, já contemplando os canais adicionais de cores, passou a utilizar aproximadamente 7.4 GB, possibilitando até o aumento do tamanho dos mini-batches para 2 sem exaurir os recursos.

\section{Preparação dos dados}

Com a adição das informações de cor na entrada da rede foi necessária a obtenção das imagens RGB dos datasets utilizados para treinamento e testes. Visto que essas imagens não estavam incluídas nos conjuntos de dados fornecidos pelo trabalho de Song et al. foi necessária a obtenção à partir das fontes. As imagens para o dataset NYU foram obtidas à partir do conjunto rotulado para segmentação semântica NYU depth v2 \cite{silberman_etal_eccv2012}. Muito esforço também foi direcionado para obter às imagens sintéticas renderizadas à partir do dataset SUNCG \cite{song_etal_SSCnet_cvpr2017}, e mais de 50 mil imagens foram renderizadas, porém a pipeline de renderização e geração dos voxels de ``groundtruth'' demandava muitos recursos e tempo, o que culminou na incapacidade de preparar os dados para este dataset em tempo hábil.

Dessa forma, nesse trabalho, apenas o dataset NYU foi utilizado para treinamento e testes da rede proposta nas suas diversas configurações.

\section{Estratégias de treinamento}

Vencida a limitação de memória e preparados os dados para o treinamento, algumas configurações distintas da rede foram escolhidas para a realização do mesmo.

\subsection{Aprendizado apenas na nova camada}

Com a adição de novos canais na camada de entrada da rede as dimensões da entrada da primeira camada convolucional foram alteradas, sendo necessário remover os pesos pré-treinados e re-treinar a camada completamente. Dessa forma, a primeira abordagem para o treinamento foi permitir o aprendizado da primeira camada convolucional, porém impedindo que qualquer outra camada seja atualizada, através da configuração do multiplicador da taxa de aprendizado para zero para todas as demais camadas que possuem pesos.

\subsection{Aprendizado ponderado de toda rede}

A segunda abordagem buscou possibilitar uma maior adaptação da rede às novas entradas de cores ao permitir que as camadas pré-treinadas recebessem atualizações nos seus pesos com 20\% da magnitude das atualizações da nova camada, através da configuração do multiplicador da taxa de aprendizado para 0.2 nessas camadas.

\subsection{``Cirurgia'' para aproveitamento dos parâmetros}

Outra abordagem focou em aproveitar os parâmetros pré-treinados da primeira camada que foi substituída, para isso foi necessária uma ``cirurgia'' na rede proposta, onde os pesos da primeira camada convolucional pré-treinada foram extraídos e enxertados no primeiro canal da nova camada que à substituiu. Tanto o bloqueio e a liberação ponderada da atualização dos pesos nas demais camadas foram experimentados em conjunto com essa abordagem.

\subsection{Aprendizado completo da rede}

Essa abordagem constitui em realizar o treinamento com a atualização integral dos pesos em todas as camadas da rede. Isso possibilita uma maior adaptação da rede ao novo tipo de entrada, porém a atualização integral de pesos nas camadas pré-treinadas pode causar uma desconfiguração das características previamente aprendidas, desfazendo parte do trabalho de pré-treino.

\subsection{Re-inicialização da rede}

Diferentemente das demais abordagens, as quais inicializavam o treinamento à partir dos pesos pré-treinados da rede SSCNet, essa abordagem parte de uma inicialização aleatória dos pesos.

\section{Resultados e Discussões}

Resultado para cada estratégia

\subsection{Discussão}